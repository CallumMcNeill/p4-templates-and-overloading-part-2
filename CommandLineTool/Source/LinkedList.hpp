//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Callum McNeill on 22/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>

class LinkedList
{
public:
    /**Constructor*/
    LinkedList();
    
    /**Destructor*/
    ~LinkedList();
    
    /**Adds new item to the end of array*/
    void add (float itemValue);
    
    /**Returns value stored in the Node at the specified index*/
    float get (int index);
    
    /**Returns the number of items currently in the array*/
    int size();
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    Node* head;
};

#endif /* LinkedList_hpp */
